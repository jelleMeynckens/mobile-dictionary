﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Dictionary
{
	[Activity (Label = "Select Dictionaries")]			
	public class DictionarySelectionActivity : Activity
	{
		Button but;
		ListView listView;
		IList<String> dictionaries;
		bool[] selectedDics;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.DictionarySelection);
			but = (Button)FindViewById (Resource.Id.submitButton);
			but.Click += (sender,e) => submit ();
			dictionaries = Intent.GetBundleExtra ("dictionaries").GetStringArrayList ("dictionaries");
			selectedDics = Intent.GetBundleExtra ("selectedDics").GetBooleanArray ("selectedDics");
			listView = (ListView)FindViewById (Resource.Id.listView);
			listView.ChoiceMode = Android.Widget.ChoiceMode.Multiple;
			fillListView ();
		}

		private void fillListView(){
			string[] dics = dictionaries.ToArray ();
			listView.Adapter = new ArrayAdapter<String> (this, Android.Resource.Layout.SimpleListItemChecked, dics);
			listView.FastScrollEnabled = true;
			selectedItems ();
		}

		private void submit(){
			var activity2 = new Intent (this, typeof(MainActivity));
			activity2.PutExtra ("MyData", getSelectedItems());
			StartActivity (activity2);
		}

		public bool [] getSelectedItems(){
			var sparseArray = FindViewById<ListView> (Resource.Id.listView).CheckedItemPositions;
			bool [] selectedDics = new bool [dictionaries.Count];

			for (int i = 0; i < sparseArray.Size (); i++) {
				selectedDics [i] = sparseArray.Get (i);
			}
			return selectedDics;
		}

		private void selectedItems(){
			var sparseArray = FindViewById<ListView> (Resource.Id.listView);
			for(int i = 0; i < selectedDics.Length; i++) {
				sparseArray.SetItemChecked (i, selectedDics [i]);
			}
			var arr = FindViewById<ListView> (Resource.Id.listView).CheckedItemPositions;
		}

	}
}

