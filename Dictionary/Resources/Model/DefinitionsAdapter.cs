﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Provider;
using Android.Views;
using Android.Widget;
using Java.IO;

namespace Dictionary
{
	public class DefinitionsAdapter : BaseAdapter<Definition> {
		IList<Definition>items;
		Activity context;

		public DefinitionsAdapter(Activity context, IList<Definition> items) : base() {
			this.context = context;
			this.items = items;
		}
		public override long GetItemId(int position)
		{
			return position;
		}
		public override Definition this[int position] {
			get { return items[position]; }
		}
		public override int Count {
			get { return items.Count; }
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView; // re-use an existing view, if one is available
			if (view == null) // otherwise create a new one
				view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
			view.FindViewById<TextView> (Android.Resource.Id.Text1).Text = items[position].toString();
			return view;
		}

		public void newList(IList<Definition> newList){
			items = newList;
			NotifyDataSetChanged ();
		}

	}

	public class Definition{

		public string dictionary{ get; set; }
		public string definition{ get; set; }
		public string toString(){
			return dictionary + ":\n" + definition;
		}
	}
}

